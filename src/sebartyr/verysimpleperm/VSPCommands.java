package sebartyr.verysimpleperm;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.Iterator;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.libs.jline.internal.Log;
import org.bukkit.entity.Player;

public class VSPCommands implements CommandExecutor{

	private VerySimplePerm plugin;
	
	public VSPCommands(VerySimplePerm plugin)
	{
		this.plugin = plugin;
		
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if((label.equalsIgnoreCase("vsp")) && sender.isOp())
		{
			 if (args[0].equalsIgnoreCase("add") && args.length == 2) {
				Player p = plugin.getServer().getPlayer(args[1]);
				if (p != null) {
					addInTheList(p);
					saveRegisteredPlayers();
					sender.sendMessage(p.getName() + " est enregistr� sur le serveur.");
					return true;
				}
				sender.sendMessage("Le joueur concern� doit �tre connect� sur le serveur pour que la commande soit ex�cut�e.");
				return true;
			}
			else if (args[0].equalsIgnoreCase("adduid") && args.length == 3) {
				String name = args[1];
				UUID uid = UUID.fromString(args[2]);
				plugin.registeredPlayers.add(new VSPPlayer(name, uid));
				saveRegisteredPlayers();
				sender.sendMessage(args[1] + " est enregistr� sur le serveur.");
				return true;
			}
			else if (args[0].equalsIgnoreCase("remove") && args.length == 2) {
				Player p = plugin.getServer().getPlayer(args[1]);
				if (p != null) {
					if (removeFromTheList(p))
					{
						sender.sendMessage(p.getName() + " n'est plus enregistr� sur le serveur.");
					}
					else
					{
						sender.sendMessage("Impossible de supprimer ce joueur, merci de contacter l'admin.");
					}
					saveRegisteredPlayers();
					return true;
				}
				sender.sendMessage("Le joueur concern� doit �tre connect� sur le serveur pour que la commande soit ex�cut�e.");
				return true;
			}
			else if (args[0].equalsIgnoreCase("list") && args.length == 1) 
			{
				sender.sendMessage("Les joueurs enregistr�s sont :");
				String res = "";
				for (VSPPlayer v : plugin.registeredPlayers)
				{
					res += v.getName() + ", ";
				}
				if (res != "")
					res = res.substring(0, res.length() - 2);
				
				sender.sendMessage(res);
				return true;
			}
		}
		if (!sender.isOp()) 
		{
			sender.sendMessage(ChatColor.DARK_RED + "Vous n'avez pas la permission d'ex�cuter cette commande");
			return true;
		}
		sender.sendMessage("Invalid command");
		return false;
	}
	
	private void saveRegisteredPlayers()
	{
		try
		{
			plugin.f.delete();
			plugin.f.createNewFile();
			ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(plugin.f)));
			oos.writeObject(plugin.registeredPlayers);
			oos.close();
		}
		catch(Exception e)
		{
			Log.error(e.toString());
		}
	}
	
	private boolean addInTheList(Player p)
	{	
		for (VSPPlayer v : plugin.registeredPlayers)
		{
			if (v.getUid().compareTo(p.getUniqueId()) == 0)
			{
				return false;
			}
		}
		return plugin.registeredPlayers.add(new VSPPlayer(p.getName(), p.getUniqueId()));
	}
	
	private boolean removeFromTheList(Player p)
	{
		boolean res = false;
		try
		{
			Iterator<VSPPlayer> iter = plugin.registeredPlayers.iterator();
			while (iter.hasNext())
			{			
				VSPPlayer v = iter.next();
				if (v.getUid().compareTo(p.getUniqueId()) == 0)
				{
					iter.remove();
					res = res || true;
				}
			}
		}
		catch(Exception e)
		{
			Log.warn(e.toString());
		}
		
		return res;
	}

}
