package sebartyr.verysimpleperm;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.libs.jline.internal.Log;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class VerySimplePerm extends JavaPlugin {

	public ArrayList<VSPPlayer> registeredPlayers;
	public File f;
	
	@Override
    public void onEnable() {
		
		this.f = new File("plugins/VerySimplePerm/players.dat");
		this.registeredPlayers = new ArrayList<VSPPlayer>();
		LoadRegisteredPlayers();
		
		PluginManager pm = getServer().getPluginManager();
		pm.registerEvents(new VSPListener(registeredPlayers), this);
		
		Log.info("Registered players");
		for (VSPPlayer v : registeredPlayers){
			
			Log.info("Name: " + v.getName() + " UID: " + v.getUid());
		}
		
		CommandExecutor cmds = new VSPCommands(this);
		getCommand("vsp").setExecutor(cmds);
		
		Log.info("VerySimplePerm enabled!");
    }
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if((label.equalsIgnoreCase("vspreload")) && sender.isOp())
		{
			if (args.length == 0) {
				registeredPlayers.clear();
				LoadRegisteredPlayers();
				sender.sendMessage("Configuration recharg�e");
				return true;
			}
		}
		if (!sender.isOp()) 
		{
			sender.sendMessage(ChatColor.DARK_RED + "Vous n'avez pas la permission d'ex�cuter cette commande");
			return true;
		}
		sender.sendMessage("Invalid command");
		return false;
	}
	
	@Override
    public void onDisable() {
		Log.info("VerySimplePerm disabled!");
    }
	
	
	
	@SuppressWarnings("unchecked")
	private void LoadRegisteredPlayers()
	{
		File p = f.getParentFile();
		
		if (!p.exists())
		{
			p.mkdir();
		}
		
		if (f.exists())
		{
			try
			{
				ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(f)));
				this.registeredPlayers.addAll((ArrayList<VSPPlayer>) ois.readObject());
				ois.close();
				
			}
			catch (Exception e)
			{
				Log.error(e.toString());
			}
		}
	}
	
	
}

