package sebartyr.verysimpleperm;

import java.io.Serializable;
import java.util.UUID;

@SuppressWarnings("serial")
public class VSPPlayer implements Serializable {
	
	private String name;
	private	UUID uid;
	
	public VSPPlayer(String name, UUID uid)
	{
		this.name = name;
		this.uid = uid;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public UUID getUid()
	{
		return this.uid;
	}

}
