package sebartyr.verysimpleperm;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;

public class VSPListener implements Listener {
	
	private ArrayList<VSPPlayer> registeredPlayers;
	
	public VSPListener(ArrayList<VSPPlayer> registeredPlayers) {
		this.registeredPlayers = registeredPlayers;
	}
	
	private boolean isInTheList(Player p) //check if the player is in the registered list 
	{
		if (registeredPlayers != null && !registeredPlayers.isEmpty())
		{
			UUID uid = p.getUniqueId();
			for (VSPPlayer pl : registeredPlayers)
			{
				if ((uid.compareTo(pl.getUid()) == 0) && (pl.getName().equals(p.getName())))
				{
					return true;
				}
			}
		}
		return false;
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {	
		Player p = e.getPlayer();
				
		if(!isInTheList(p) && !p.isOp())
		{
			p.sendMessage("Vous �tes un visiteur, merci de vous enregistrer aupr�s de l'admin.");
		}
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e) {
		Player p = e.getPlayer();
		
		if(!isInTheList(p) && !p.isOp())
		{
			p.sendMessage("Vous ne pouvez pas faire cette action.");
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onBlockDamageEvent(BlockDamageEvent e) {
		Player p = e.getPlayer();
		
		if(!isInTheList(p) && !p.isOp())
		{
			p.sendMessage("Vous ne pouvez pas faire cette action.");
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPlayerInteractEvent(PlayerInteractEvent e) {	
		Player p = e.getPlayer();
				
		if(!isInTheList(p) && !p.isOp())
		{
			p.sendMessage("Vous ne pouvez pas faire cette action.");
			e.setCancelled(true);
		}
	}
}
